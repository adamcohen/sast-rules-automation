// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/ajinabraham/njsscan/blob/master/tests/assets/node_source/true_positives/semantic_grep/xss/xss_serialize_js.js
// hash: e7a0a61
var serialize = require('serialize-javascript');

function test(userInput) {
    // ruleid: rules_lgpl_javascript_xss_rule-xss-serialize-javascript
    const result = serialize({ foo: userInput }, { unsafe: true, space: 2 })
    return result
}

function test2() {
    // ruleid: rules_lgpl_javascript_xss_rule-xss-serialize-javascript
    const result = serialize({ foo: '<img src=x />' }, { unsafe: true, space: 2 })
    return result
}
