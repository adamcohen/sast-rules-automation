# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/mako_templating.py
# hash:  8eee173

from mako.template import Template
import mako

from mako import template

# ruleid: python_escaping_rule-use-of-mako-templates
Template("hello")

# XXX(fletcher): for some reason, bandit is missing the one below. keeping it
# in for now so that if it gets fixed inadvertitently we know.
# ruleid: python_escaping_rule-use-of-mako-templates
mako.template.Template("hern")
# ruleid: python_escaping_rule-use-of-mako-templates
template.Template("hern")
