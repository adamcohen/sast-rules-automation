# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import subprocess as subp

# Vulnerable to wildcard injection
# ruleid: python_exec_rule-linux-command-wildcard-injection
o.system("/bin/tar xvzf *")
# ruleid: python_exec_rule-linux-command-wildcard-injection
o.system('/bin/chown *')
# ruleid: python_exec_rule-linux-command-wildcard-injection
o.popen2('/bin/chmod *')
# ruleid: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/chown *', shell=True)

# Not vulnerable to wildcard injection
subp.Popen('/bin/rsync *')
subp.Popen("/bin/chmod *")
subp.Popen(['/bin/chown', '*'])
subp.Popen(["/bin/chmod", sys.argv[1], "*"],
                 stdin=subprocess.PIPE, stdout=subprocess.PIPE)
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])
