// License: MIT (c) GitLab Inc.
#include <openssl/evp.h>

#include <stdio.h>

void main(void) {

  // ruleid: c_crypto_rule-EVP-des-ecb-EVP-des-cbc
  EVP_des_cbc(void), EVP_des_ecb(void), EVP_des_cfb(void), EVP_des_ofb(void), EVP_desx_cbc(void);

}
