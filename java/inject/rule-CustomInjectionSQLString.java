// License: MIT (c) GitLab Inc.

package inject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class CustomInjectionSQLString {

    public List<String> callVulnerableCodePatterns(String input) {

        List<String> responses = new ArrayList<>();

        String sql;

        // Sample exploit :
        // http://localhost:8080/ServletSample/TestSQLInjection?name=Bob%27%20or%20id=%271

        // ruleid: java_inject_rule-CustomInjectionSQLString
        sql = "select id, name, email from users where name = '" + input + "'";
        responses.add(queryDataUnsafely(sql));

        // ok: java_inject_rule-CustomInjectionSQLString
        sql = "select id, name, email from users";
        responses.add(queryDataUnsafely(sql));

        // ruleid: java_inject_rule-CustomInjectionSQLString
        sql = String.format("Select * from users where name = '%s'", input);
        responses.add(queryDataUnsafely(sql));

        // ok: java_inject_rule-CustomInjectionSQLString
        sql = String.format("Select * from users");
        responses.add(queryDataUnsafely(sql));

        sql = "Select * from users";
        // ok: java_inject_rule-CustomInjectionSQLString
        sql += " where name = 'Alice'";
        responses.add(queryDataUnsafely(sql));

        sql = "Select * from users";
        // ruleid: java_inject_rule-CustomInjectionSQLString
        sql += " where name = '" + input + "'";
        responses.add(queryDataUnsafely(sql));

        // ruleid: java_inject_rule-CustomInjectionSQLString
        StringBuilder sb = new StringBuilder("select * from users where name = '" + input + "'");
        responses.add(queryDataUnsafely(sb.toString()));

        StringBuilder sb2 = new StringBuilder("select * from users ");
        // ruleid: java_inject_rule-CustomInjectionSQLString
        sb2.append("where name = '" + input + "'");
        responses.add(queryDataUnsafely(sb2.toString()));

        StringBuilder sb3 = new StringBuilder("select * from users ");
        // ok: java_inject_rule-CustomInjectionSQLString
        sb3.append("where name = 'Alice'");
        responses.add(queryDataUnsafely(sb3.toString()));

        StringBuilder sb4 = new StringBuilder();
        // ruleid: java_inject_rule-CustomInjectionSQLString
        sb4.append("select * from users where name = '" + input + "'");
        responses.add(queryDataUnsafely(sb4.toString()));

        // ok: java_inject_rule-CustomInjectionSQLString
        new StringBuilder("this is ").append(input).append(" totally unrelated.").toString();

        return responses;
    }

    public void logStuff(String input) {

        Logger log = Logger.getLogger(CustomInjectionSQLString.class.getName());

        // ok: java_inject_rule-CustomInjectionSQLString
        String result2 = "insert into log file failed " + input;
        log.info(result2);

        // ok: java_inject_rule-CustomInjectionSQLString
        String result = "select user failed " + input;
        log.warning(result);

        // ok: java_inject_rule-CustomInjectionSQLString
        log.severe("select user failed " + input);

        // ok: java_inject_rule-CustomInjectionSQLString
        log.info(String.format("Select random Name %s", input));

    }

    public String queryDataUnsafely(String sql) {
        // execute query
        return "query result string";
    }
}
